package com.example.demobe.dto;

import lombok.Data;

@Data
public class UserDto {
  private Integer id;
  private String name;
  private String surname;
  private String email;
  private String address;
}

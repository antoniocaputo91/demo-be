package com.example.demobe.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse {

	private Integer status;
	private String message;
	private String stackTrace;
}

package com.example.demobe.dto;

import lombok.Builder;
import lombok.Data;

@Data
public class NewUserResponse {
  private Integer id;

  @Builder
  public NewUserResponse(Integer id) {
    this.id = id;
  }
}

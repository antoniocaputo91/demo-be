package com.example.demobe.service;

import com.example.demobe.dao.UserDao;
import com.example.demobe.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

  @Autowired private UserDao dao;

  public User getUserById(Integer id) {
    return dao.getById(id);
  }

  public User createOrUpdateUser(User user) {
    dao.save(user);
    dao.flush();
    return user;
  }

  public void deleteUser(Integer id) {
    dao.deleteById(id);
  }

  public List<User> searchUser(String name, String surname) {
    if (isFieldValued(name) && isFieldValued(surname))
      return dao.findByNameAndSurname(name, surname);
    else if (isFieldValued(name)) return dao.findByName(name);
    else if (isFieldValued(surname)) return dao.findBySurname(surname);
    else return dao.findAll();
  }

  private boolean isFieldValued(String field) {
    return field != null && !field.equals("");
  }
}

package com.example.demobe.integration;

import com.example.demobe.dto.NewUserDto;
import com.example.demobe.dto.NewUserResponse;
import com.example.demobe.dto.UpdateUserDto;
import com.example.demobe.dto.UserDto;
import com.example.demobe.entity.User;
import com.example.demobe.mapper.UserMapper;
import com.example.demobe.service.UserService;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class UserIntegration {

  @Autowired private UserService service;

  @Autowired private UserMapper mapper;

  public UserDto getUser(Integer id) {
    User user = service.getUserById(id);
    return mapper.userToDto(user);
  }

  public void deleteUser(Integer id) {
    service.deleteUser(id);
  }

  public NewUserResponse createUser(NewUserDto newUser) throws Exception {
    if (isEmailValid(newUser.getEmail())) throw new Exception("Email not valid");
    User user = mapper.newUserDtoToUser(newUser);
    Integer id = service.createOrUpdateUser(user).getId();
    return NewUserResponse.builder().id(id).build();
  }

  public void updateUser(UpdateUserDto updateUserDto) throws Exception {
    if (isEmailValid(updateUserDto.getEmail())) throw new Exception("Email not valid");
    User user = mapper.updateUserDtoToUser(updateUserDto);
    service.createOrUpdateUser(user);
  }

  public List<UserDto> searchUsers(String name, String surname) {
    List<User> users = service.searchUser(name, surname);
    return mapper.userToDto(users);
  }

  private boolean isEmailValid(String email) {
    return !EmailValidator.getInstance().isValid(email);
  }
}

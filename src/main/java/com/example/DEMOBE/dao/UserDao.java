package com.example.demobe.dao;

import com.example.demobe.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

  public List<User> findByName(String name);

  public List<User> findBySurname(String surname);

  public List<User> findByNameAndSurname(String name, String surname);
}

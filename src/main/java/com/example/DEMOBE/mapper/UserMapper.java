package com.example.demobe.mapper;

import com.example.demobe.dto.NewUserDto;
import com.example.demobe.dto.UpdateUserDto;
import com.example.demobe.dto.UserDto;
import com.example.demobe.entity.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
  UserDto userToDto(User user);

  List<UserDto> userToDto(List<User> user);

  User newUserDtoToUser(NewUserDto user);

  User updateUserDtoToUser(UpdateUserDto updateUserDto);

  User dtoToUser(UserDto user);

  List<User> dtoToUser(List<UserDto> users);
}

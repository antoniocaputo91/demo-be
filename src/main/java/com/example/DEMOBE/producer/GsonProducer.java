package com.example.demobe.producer;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.*;
import java.time.format.DateTimeFormatter;

public class GsonProducer {
	public Gson getGson() {
		return new GsonBuilder()
		.serializeNulls()
		.registerTypeAdapter(
		LocalDateTime.class,
		(JsonSerializer<LocalDateTime>)
		(date, typeOfSrc, context) ->
		new JsonPrimitive(
		date.atOffset(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT)))
		.registerTypeAdapter(
		LocalDate.class,
		(JsonSerializer<LocalDate>)
		(date, typeOfSrc, context) ->
		new JsonPrimitive(date.format(DateTimeFormatter.ISO_LOCAL_DATE)))
		.registerTypeAdapter(LocalDateTime.class, new CustomLocalDateTimeDeserializer())
		.registerTypeAdapter(
		LocalDate.class,
		(JsonDeserializer<LocalDate>)
		(jsonElement, typeOfSrc, context) ->
		LocalDate.parse(jsonElement.getAsJsonPrimitive().getAsString()))
		.create();
	}

	private class CustomLocalDateTimeDeserializer implements JsonDeserializer<LocalDateTime> {

		@Override
		public LocalDateTime deserialize(
		JsonElement json, Type typeOfT, JsonDeserializationContext context) {
			try {
				Double asDouble = json.getAsJsonPrimitive().getAsDouble() * 1000;
				return Instant.ofEpochMilli(asDouble.longValue())
				.atZone(ZoneId.systemDefault())
				.toLocalDateTime();
			} catch (Exception e) {
				try {
					String asString = json.getAsJsonPrimitive().getAsString();
					if (asString.endsWith("Z") || asString.contains("+")) {
						return OffsetDateTime.parse(asString)
						.withOffsetSameInstant(ZoneOffset.UTC)
						.toLocalDateTime();
					} else {
						return LocalDateTime.parse(asString);
					}
				} catch (Exception ex) {
					throw new JsonParseException("Could not parse to date: " + json, ex);
				}
			}
		}
	}
}

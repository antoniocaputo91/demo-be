package com.example.demobe.rest;

import com.example.demobe.dto.NewUserDto;
import com.example.demobe.dto.NewUserResponse;
import com.example.demobe.dto.UpdateUserDto;
import com.example.demobe.dto.UserDto;
import com.example.demobe.integration.UserIntegration;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserRestController extends BaseRestController {

  @Autowired private Gson gson;

  @Autowired private UserIntegration integration;

  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> getUser(@PathVariable("id") Integer id) {
    UserDto user = integration.getUser(id);
    String response = gson.toJson(user);
    return ResponseEntity.ok().body(response);
  }

  @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> deleteUser(@PathVariable("id") Integer id) {
    integration.deleteUser(id);
    return ResponseEntity.ok().build();
  }

  @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> updateUser(@RequestBody UpdateUserDto request) throws Exception {
    integration.updateUser(request);
    return ResponseEntity.ok().build();
  }

  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> createUser(@RequestBody NewUserDto request) throws Exception {
    NewUserResponse response = integration.createUser(request);
    return ResponseEntity.ok(gson.toJson(response));
  }

  @GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> searchUser(
      @RequestParam(required = false, value = "name") String name,
      @RequestParam(required = false, value = "surname") String surname) {
    List<UserDto> users = integration.searchUsers(name, surname);
    return ResponseEntity.ok(gson.toJson(users));
  }
}

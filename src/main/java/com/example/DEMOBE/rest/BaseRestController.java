package com.example.demobe.rest;

import com.example.demobe.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import javax.persistence.EntityNotFoundException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BaseRestController {

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ResponseEntity<ErrorResponse> handleException(Exception exception, WebRequest request) {
    return buildErrorResponse(exception, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<ErrorResponse> handleEntityNotFoundException(
      Exception exception, WebRequest request) {
    return buildErrorResponse(exception, HttpStatus.NOT_FOUND);
  }

  private ResponseEntity<ErrorResponse> buildErrorResponse(
      Exception exception, HttpStatus httpStatus) {
    ErrorResponse errorResponse =
        new ErrorResponse(httpStatus.value(), exception.getMessage(), getStackTrace(exception));
    return ResponseEntity.status(httpStatus).body(errorResponse);
  }

  private String getStackTrace(Exception e) {
    return Stream.of(e.getStackTrace())
        .map(StackTraceElement::toString)
        .collect(Collectors.joining("\n"));
  }
}

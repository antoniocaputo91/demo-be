package com.example.demobe.utils;

import com.example.demobe.dto.NewUserDto;
import com.example.demobe.dto.UpdateUserDto;
import com.example.demobe.entity.User;

public class MockUtils {

  public static NewUserDto mockNewUserDto(String email) {
    NewUserDto newUserDto = new NewUserDto();
    newUserDto.setName("test");
    newUserDto.setSurname("test");
    newUserDto.setAddress("test");
    newUserDto.setEmail(email);
    return newUserDto;
  }

  public static UpdateUserDto mockUpdateUserDto(String email) {
    UpdateUserDto updateUserDto = new UpdateUserDto();
    updateUserDto.setId(1);
    updateUserDto.setName("test");
    updateUserDto.setSurname("test");
    updateUserDto.setAddress("test");
    updateUserDto.setEmail(email);
    return updateUserDto;
  }

  public static User mockUser() {
    User user = new User();
    user.setId(1);
    user.setAddress("test");
    user.setName("test");
    user.setSurname("test");
    user.setEmail("test");
    return user;
  }
}

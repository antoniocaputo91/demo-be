package com.example.demobe.integration;

import com.example.demobe.dto.NewUserDto;
import com.example.demobe.dto.UpdateUserDto;
import com.example.demobe.entity.User;
import com.example.demobe.mapper.UserMapper;
import com.example.demobe.service.UserService;
import com.example.demobe.utils.MockUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserIntegrationTests {
  @InjectMocks private UserIntegration integration;

  @Mock private UserMapper mapper;

  @Mock private UserService service;

  @Test
  void testUserCreateWithWrongEmail() throws Exception {
    NewUserDto user = MockUtils.mockNewUserDto("prova");
    Assertions.assertThrows(
        Exception.class,
        () -> {
          integration.createUser(user);
        });
  }

  @Test
  void testUserCreateWithRightEmail() throws Exception {
    NewUserDto newUser = MockUtils.mockNewUserDto("prova@prova.it");
    User user = MockUtils.mockUser();
    Mockito.doReturn(user).when(mapper).newUserDtoToUser(newUser);
    Mockito.when(service.createOrUpdateUser(user)).thenReturn(user);

    Assertions.assertAll(() -> integration.createUser(newUser));
  }

  @Test
  void testUserUpdateWithWrongEmail() throws Exception {
    UpdateUserDto user = MockUtils.mockUpdateUserDto("prova");
    Assertions.assertThrows(
        Exception.class,
        () -> {
          integration.updateUser(user);
        });
  }

  @Test
  void testUserUpdateWithRightEmail() throws Exception {
    UpdateUserDto updateUser = MockUtils.mockUpdateUserDto("prova@prova.it");
    User user = MockUtils.mockUser();
    Mockito.doReturn(user).when(mapper).updateUserDtoToUser(updateUser);
    Mockito.when(service.createOrUpdateUser(user)).thenReturn(user);

    Assertions.assertAll(() -> integration.updateUser(updateUser));
  }
}

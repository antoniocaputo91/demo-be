# DEMO

## Network

Before all it's necessary create a docker network

```
docker network create local
```

We need it for run our database and java image docker.

## Database

### Docker run

The database oracle will be configured with Docker:

```
docker run -d --name oracledb_demo -p 1521:1521 --network local -v oracledb_volume:/ORCL_DEMO store/oracle/database-enterprise:12.2.0.1-slim
```

### Database configuration

This is the configuration to access the database like sysdba

```
driver=oracle.jdbc.driver.OracleDriver
url=jdbc:oracle:thin:@//127.0.0.1:1521/orclpdb1.localdomain
user=sys as sysdba
password=Oradoc_db1
```

After access, it's necessary create a new user

```
CREATE TABLESPACE tbsp1 DATAFILE 'tbsp1.dbf' SIZE 500m;
CREATE USER demo IDENTIFIED BY "demo" default tablespace tbsp1 quota 500m on tbsp1;
grant create session to demo;
grant create table to demo;
grant create view,
create procedure,
create sequence to demo;
```

## Project

### Build with docker

After made a maven clean install of application

```
mvn clean install
```

This will create an executable jar file within the target maven folder. This can be build by executing the following command

```
docker build --build-arg JAR_FILE=target/*.jar -t example/demo .
```

And then run by

```
docker run --name demobe -p 8080:8080 --network local -v demo_volume/DEMO example/demo
```